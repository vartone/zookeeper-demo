package com.chatly.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;


/**
 * @author lihuazeng
 */
public class Curator4Zk {
    private static String ZK_SERVER = "127.0.0.1:2181";

    public static void main(String[] args) {
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder().connectString(ZK_SERVER)
                .sessionTimeoutMs(5000).retryPolicy(new ExponentialBackoffRetry(1000,3)).build();
        curatorFramework.start();

        String createdPath = create(curatorFramework);
        System.out.println("创建了节点目录："+createdPath);

        if (createdPath != null) {
            int v = update(curatorFramework, createdPath, "testUpdate");
            System.out.println("修改后此节点的版本号："+v);
        }

        NodeData data = null;
        if (createdPath != null) {
            data = get(curatorFramework, createdPath);
        }
        if (data != null) {
            System.out.println("查询节点数据："+data.getValue());
        }

        if (data != null) {
            boolean deleted = delete(curatorFramework, createdPath, data.getStat().getVersion());
            System.out.println("删除节点是否成功："+deleted);
        }

    }


    /**
     * 根据版本号删除（多级目录）节点
     *
     * @param curatorFramework
     * @param createdPath
     * @param version
     * @return
     */
    private static boolean delete(CuratorFramework curatorFramework, String createdPath, int version) {
        try {
            curatorFramework.delete().withVersion(version).forPath(createdPath);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 创建多级目录持久化节点
     *
     * @param curatorFramework
     * @return
     */
    public static String create(CuratorFramework curatorFramework) {
        try {
            return curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT)
                    .forPath("/demo/curator","test".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定节点的信息
     *
     * @param curatorFramework
     * @return
     */
    public static NodeData get(CuratorFramework curatorFramework, String path) {
        try {
            Stat stat = new Stat();
            String value = new String(curatorFramework.getData().storingStatIn(stat).forPath(path));
            return new NodeData(stat, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 更新节点的值
     *
     * @param curatorFramework
     * @param path
     * @param value
     * @return
     */
    public static int update(CuratorFramework curatorFramework, String path, String value) {
        try {
            Stat stat = curatorFramework.setData().forPath(path, value.getBytes());
            return stat.getVersion();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


}


class NodeData {
    private Stat stat;
    private String value;

    public NodeData(Stat stat, String value) {
        this.stat = stat;
        this.value = value;
    }

    public Stat getStat() {
        return stat;
    }

    public void setStat(Stat stat) {
        this.stat = stat;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
