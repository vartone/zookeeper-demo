package com.chatly.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author lihuazeng
 */
public class CuratorLock {
    private static String ZK_SERVER = "127.0.0.1:2181";

    public static void main(String[] args) {
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder().connectString(ZK_SERVER)
                .sessionTimeoutMs(5000).retryPolicy(new ExponentialBackoffRetry(1000,3)).build();
        curatorFramework.start();

        //InterProcessMutex 这个锁为可重入锁
        InterProcessMutex interProcessMutex = new InterProcessMutex(curatorFramework, "/locks");
        ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            fixedThreadPool.submit(() -> {
                boolean flag = false;
                try {
                    //尝试获取锁，最多等待5秒
                    flag = interProcessMutex.acquire(5, TimeUnit.SECONDS);
                    Thread currentThread = Thread.currentThread();
                    if(flag){
                        System.out.println("线程"+currentThread.getId()+"获取锁成功");
                    }else{
                        System.out.println("线程"+currentThread.getId()+"获取锁失败");
                    }
                    //模拟业务逻辑，延时2秒
                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally{
                    if(flag){
                        try {
                            //释放锁
                            interProcessMutex.release();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        // 延时20秒,输出完后退出
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
